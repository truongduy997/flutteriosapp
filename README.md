 - create module flutter into project IOS
 - run flutter create --template module my_flutter in project path
 - run flutter run --debug or flutter build ios


**Embed with CocoaPods and the Flutter SDK**

- add a Podfile to your project
- add the 2 lines in header Podfile

        flutter_application_path = '../my_flutter'
        load File.join(flutter_application_path, '.ios', 'Flutter', 'podhelper.rb')

- for each Podfile target that needs to embed Flutter, call

        target 'MyApp' do
            install_all_flutter_pods(flutter_application_path)
        end

- the proper place to create a FlutterEngine is specific to your host app
    in AppDelegate

        lazy var flutterEngine = FlutterEngine(name: "my flutter engine")
        flutterEngine.run();
        GeneratedPluginRegistrant.register(with: self.flutterEngine);

**Show flutter view**

- Paste this code into ViewController you want show Flutter view

        @objc func showFlutter() {
            let flutterEngine = (UIApplication.shared.delegate as! AppDelegate).flutterEngine
            let flutterViewController = FlutterViewController(engine: flutterEngine, nibName: nil, bundle: nil)
            self.present(flutterViewController, animated: true, completion: nil)
        }
      
  

        