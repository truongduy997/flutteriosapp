//
//  AppDelegate.swift
//  my_flutter
//
//  Created by Apple on 3/9/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Flutter
import FlutterPluginRegistrant

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    static var appDelegate: AppDelegate? = nil
    
    var flutterEngine = FlutterEngine(name: "my flutter engine")
    
    var flutterViewController : FlutterViewController!
    
    var flutterMethodChannel: FlutterMethodChannel!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        flutterEngine.run();
        self.flutterViewController = FlutterViewController(engine: AppDelegate.shared().flutterEngine, nibName: nil, bundle: nil)
        self.flutterMethodChannel = FlutterMethodChannel(name: "com.flutter.my-flutter/data",
                                                              binaryMessenger:  self.flutterViewController.binaryMessenger)
        GeneratedPluginRegistrant.register(with: self.flutterEngine);
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    class func shared() -> AppDelegate {
          if appDelegate == nil {
              appDelegate = (UIApplication.shared.delegate as! AppDelegate)
          }
          return appDelegate!
      }
      

}

