import 'package:flutter/material.dart';
import 'package:my_flutter/details.dart';

import 'main.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case "/home":
        return MaterialPageRoute<dynamic>(builder: (_) => HomePage());
      case "/details":
        return MaterialPageRoute<dynamic>(builder: (_) => Details());
      default:
        return _errorRoute(settings.name);
    }
  }

  static Route<dynamic> _errorRoute(String route) {
    return MaterialPageRoute<dynamic>(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Not found'),
        ),
        body: Center(
          child: Text('Error routes to $route'),
        ),
      );
    });
  }
}