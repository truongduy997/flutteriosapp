import 'package:flutter/material.dart';
import 'package:my_flutter/main.dart';

class Details extends StatefulWidget {
  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  
  
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Details - Flutter"),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: FlatButton(
          onPressed: invokeMethodFlutter,
          child: Text("comeback IOS Native"),
        ),
      ),
    );
  }

  void invokeMethodFlutter() {
    MyApp.platform.invokeMethod("fromClientToHost", "data Flutter");
  }
}
