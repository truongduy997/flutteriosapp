
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_flutter/router.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const platform = const MethodChannel('com.flutter.my-flutter/data');


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: "/",
      onGenerateRoute: RouteGenerator.generateRoute,
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  Future<void> _receiveFromHost(MethodCall call) async {
    try {
      switch (call.method){
        case "fromHostToClient":
          final String data = call.arguments;
          print(data);
          break;
      }
    } on PlatformException catch (error) {
      print(error);
    }
  }

  @override
  void initState() {
    MyApp.platform.setMethodCallHandler(_receiveFromHost);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home - Flutter"),
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: Center(
          child: FlatButton(
            onPressed: () => Navigator.pushNamed(context, "/details"),
            child: Text("Button - Flutter"),
          )
        ),
      ),
    );
  }
}

