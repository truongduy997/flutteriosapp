//
//  ViewController.swift
//  my_flutter
//
//  Created by Apple on 3/9/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Flutter


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func action(_ sender: UIButton) {
        AppDelegate.shared().flutterMethodChannel.invokeMethod("fromHostToClient", arguments: "data IOS")
        let screenTwo: ScreenTwo = self.storyboard?.instantiateViewController(identifier: "ScreenTwo") as! ScreenTwo
        AppDelegate.shared().flutterMethodChannel.setMethodCallHandler({
            (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            switch(call.method) {
            case "fromClientToHost":
                print(call.arguments!)
                self.dismiss(animated: true) {
                    self.navigationController?.pushViewController(screenTwo, animated: true)
                }
                break;
            default:
                break;
            }
          })
        
//        self.navigationController?.pushViewController(AppDelegate.shared().flutterViewController, animated: true)
        self.present(AppDelegate.shared().flutterViewController, animated: true, completion: nil)
    }
}

