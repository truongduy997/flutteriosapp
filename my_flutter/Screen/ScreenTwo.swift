//
//  ScreenTwo.swift
//  my_flutter
//
//  Created by Apple on 3/9/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ScreenTwo: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func action(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
}
